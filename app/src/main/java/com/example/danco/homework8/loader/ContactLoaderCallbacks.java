package com.example.danco.homework8.loader;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.example.danco.homework8.provider.ContactContract;

import java.lang.ref.WeakReference;

/**
 * Created by danco on 3/7/15.
 */
public class ContactLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "ContactLoaderCD";

    private static final String ARG_PROJECTION = "projection";
    private static final String ARG_URI = "uri";

    private final Context applicationContext;
    private final WeakReference<ContactLoadListener> listenerRef;

    public interface ContactLoadListener {
        public void onContactLoadComplete(Cursor cursor);
    }


    /**
     * Initialize a loader.
     * Call from an Activity's onCreate or Fragment's onActivityCreated method.
     */
    public static void initLoader(Context context, LoaderManager loaderManager,
                                  ContactLoadListener listener, String[] projection) {
        Bundle args = new Bundle();
        args.putStringArray(ARG_PROJECTION, projection);

        // Every loader needs a unique id per the current activity/fragment.
        loaderManager.initLoader(LoaderIds.CONTACT_LOADER, args,
                new ContactLoaderCallbacks(context, listener));
    }

    /**
     * Initialize a loader to query a specific contact using the provided contact id.
     */
    public static void initLoader(Context context, LoaderManager loaderManager,
                                  ContactLoadListener listener, String[] projection,
                                  long contactId) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_URI, ContentUris.withAppendedId(ContactContract.URI, contactId));
        args.putStringArray(ARG_PROJECTION, projection);
        loaderManager.initLoader(LoaderIds.CONTACT_LOADER,
                args, new ContactLoaderCallbacks(context, listener));
    }

    /**
     * This method is here for demonstration purposes and is not required for this type of app.
     * If the method took a where clause, it would allow the current activity/fragment to
     * discard the current results and issue a new query. Useful if you were doing a "search" where
     * the user's input as they typed was used as a "filter" in the query or something else
     * along those lines.
     */
    public static void restartLoader(Context context, LoaderManager loaderManager,
                                     ContactLoadListener listener, String[] projection) {
        Bundle args = new Bundle();
        args.putStringArray(ARG_PROJECTION, projection);
        // Every loader needs a unique id per the current activity/fragment.
        loaderManager.restartLoader(LoaderIds.CONTACT_LOADER, args,
                new ContactLoaderCallbacks(context, listener));
    }

    /**
     * This method is here for demonstration purposes and is not required for this type of app.
     * If the activity/fragment needs to destroy the loader earlier than the "automatic" destruction.
     * Might be useful if you are placing a large number of fragments on the "back stack" as the fragment
     * will not be destroyed, but you may want to "cleanup" the memory the loader is holding manually.
     *
     * However, per the fragment guide, the lifecycle will ensure that "onActivityCreated()" is
     * called when the fragment is restored from the back stack. This is another reason it is recommended
     * to initialize your loaders in this method when using fragments.
     * http://developer.android.com/guide/components/fragments.html
     */
    public static void destroyLoader(LoaderManager loaderManager) {
        // Every loader needs a unique id per the current activity/fragment.
        loaderManager.destroyLoader(LoaderIds.CONTACT_LOADER);
    }

    /**
     * Private constructor to prevent direct instantiation
     * @param context the context to use for starting the loader
     * @param listener the listener to use to provide contact cursor data back to the caller
     */
    private ContactLoaderCallbacks(Context context, ContactLoadListener listener) {
        this.applicationContext = context.getApplicationContext();
        this.listenerRef = new WeakReference<>(listener);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader");
        Uri uri = args.getParcelable(ARG_URI);
        if (uri == null) {
            uri = ContactContract.URI;
        }

        // In this case, we are starting a cursor loader. Notice how this syncs nicely with
        // the content resolver's query call.
        return new CursorLoader(
                applicationContext,
                uri,
                args.getStringArray(ARG_PROJECTION),
                null,
                null,
                ContactContract.Columns.NAME + " asc");
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.d(TAG, "onLoadFinished result is " + (cursor != null ? "not null" : "null"));

        // Have a result. Note, it may be null, but we want to tell our listener if
        // we still have one with the value regardless.
        ContactLoadListener listener = listenerRef.get();
        if (listener != null) {
            Log.d(TAG, "onLoadFinished. Notifying listener");
            listener.onContactLoadComplete(cursor);
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoadReset");

        // Since we just let the listener know the value is null, call into onLoadFinished
        // to avoid duplicating listener logic.
        onLoadFinished(loader, null);
    }
}
