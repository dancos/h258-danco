package com.example.danco.homework8.activity;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.danco.homework8.R;
import com.example.danco.homework8.fragment.ContactDetailFragment;
import com.example.danco.homework8.provider.ContactContract;
import com.example.danco.homework8.service.ContactService;

public class ContactDetailActivity extends ActionBarActivity {

    private static final String EXTRA_CONTACT_ID =
            ContactDetailActivity.class.getName() + ".contactId";

    public static Intent buildIntent(Context context, long contactId) {
        Intent intent = new Intent(context, ContactDetailActivity.class);
        intent.putExtra(EXTRA_CONTACT_ID, contactId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Use a boolean to indicate multi fragment possible. If so, finish and
        // quit processing.
        boolean multiFragment = getResources().getBoolean(R.bool.multiFragment);
        if (multiFragment) {
            finish();
            return;
        }

        // Must be phone, so load our hierarchy now.
        setContentView(R.layout.activity_contact_detail);

        setSupportActionBar((Toolbar) findViewById(R.id.contactDetailToolBar));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Only add a dynamic fragment if the saved instance state is null.
        // Otherwise it already has been added to the view hierarchy
        if (savedInstanceState == null) {
            long id = getIntent().getLongExtra(EXTRA_CONTACT_ID, 0);
            ContactDetailFragment fragment = ContactDetailFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.contactDetailContainer, fragment, "DETAILS")
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_contact_detail, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
            case R.id.action_delete:
                long contactId = getIntent().getLongExtra(EXTRA_CONTACT_ID, 0);
                ContactService.startActionDelete(this, ContentUris.withAppendedId(ContactContract.URI, contactId));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
