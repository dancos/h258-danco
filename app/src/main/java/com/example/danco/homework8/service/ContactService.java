package com.example.danco.homework8.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.example.danco.homework8.provider.ContactContract;

import java.util.Calendar;

/**
 * Created by danco on 3/7/15.
 */
public class ContactService extends IntentService {
    private static final String ACTION_CREATE = ContactService.class.getName() + ".CREATE";
    private static final String ACTION_INSERT = ContactService.class.getName() + ".INSERT";
    private static final String ACTION_UPDATE = ContactService.class.getName() + ".UPDATE";
    private static final String ACTION_DELETE = ContactService.class.getName() + ".DELETE";

    private static final String EXTRA_URI = ContactService.class.getName() + ".uri";
    private static final String EXTRA_VALUES = ContactService.class.getName() + ".values";

    private static final String[] ID_PROJECTION = new String[]{BaseColumns._ID};

    private static boolean complete = false;


    /**
     * Starts this service to perform action create. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionCreate(Context context) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_CREATE);
        context.startService(intent);
    }


    public static void startActionInsert(Context context, ContentValues values) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_INSERT);
        intent.putExtra(EXTRA_VALUES, values);
        context.startService(intent);
    }


    public static void startActionUpdate(Context context, Uri uri, ContentValues values) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_UPDATE);
        intent.putExtra(EXTRA_URI, uri);
        intent.putExtra(EXTRA_VALUES, values);
        context.startService(intent);
    }


    public static void startActionDelete(Context context, Uri uri) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_DELETE);
        intent.putExtra(EXTRA_URI, uri);
        context.startService(intent);
    }


    public ContactService() {
        super("StartService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
             // Only run once per start to avoid db overhead since initial data is static
             // to this class
            if (!complete && ACTION_CREATE.equals(action)) {
                handleActionCreate();
            } else if (ACTION_INSERT.equals(action)) {
                Uri uri = intent.getParcelableExtra(EXTRA_URI);
                ContentValues values = intent.getParcelableExtra(EXTRA_VALUES);
                handleActionInsert(values);
            } else if (ACTION_UPDATE.equals(action)) {
                Uri uri = intent.getParcelableExtra(EXTRA_URI);
                ContentValues values = intent.getParcelableExtra(EXTRA_VALUES);
                handleActionUpdate(uri, values);
            } else if (ACTION_DELETE.equals(action)) {
                Uri uri = intent.getParcelableExtra(EXTRA_URI);
                handleActionDelete(uri);
            }
        }
    }


    /**
     * Handle action in the provided background thread with the provided parameters.
     */
    private void handleActionCreate() {
        Cursor cursor = getContentResolver().query(ContactContract.URI, ContactService.ID_PROJECTION, null, null, null);
        if (cursor.getCount() == 0) {
            int rows = getContentResolver().bulkInsert(ContactContract.URI, buildContactValuesArray());
            complete = rows > 0;
        }
    }


    private void handleActionInsert(ContentValues values) {
        getContentResolver().insert(ContactContract.URI, values);
    }


    private void handleActionUpdate(Uri uri, ContentValues values) {
        getContentResolver().update(uri, values, null, null);
    }


    private void handleActionDelete(Uri uri) {
        getContentResolver().delete(uri, null, null);
    }


    /**
     * default state values. In real world this should come from a file or the
     * internet
     *
     * @return state values array
     */
    private ContentValues[] buildContactValuesArray() {
        final String address = "123 Demo Street, Seattle WA 98104";
        return new ContentValues[]{
                buildStateValues("John Doe", address),
                buildStateValues("Susie Homemaker",address),
        };
    }

    private ContentValues buildStateValues(String name, String address) {
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, name);
        values.put(ContactContract.Columns.ADDRESS, address);
        Calendar rightNow = Calendar.getInstance();
        values.put(ContactContract.Columns.BIRTH_DATE, rightNow.getTimeInMillis());
        return values;
    }
}
