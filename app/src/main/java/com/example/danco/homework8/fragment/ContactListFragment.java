package com.example.danco.homework8.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.danco.homework8.R;
import com.example.danco.homework8.adapter.ContactListAdapter;
import com.example.danco.homework8.loader.ContactLoaderCallbacks;


public class ContactListFragment extends Fragment
        implements AdapterView.OnItemClickListener,
        ContactLoaderCallbacks.ContactLoadListener {
    public static final String TAG = "DRC";

    private static final String ARGS_CHOICE_MODE = "listChoiceMode";
    private static final String STATE_SELECTION =
            ContactListFragment.class.getSimpleName() + ".selection";

    private int listChoiceMode = ListView.CHOICE_MODE_NONE;
    private int selectedItem = 0;
    private ContactListFragmentListener listener;
    private int animationDuration;


    public interface ContactListFragmentListener {
        public void editContact(long contactId);
    }


    public static ContactListFragment newInstance(boolean highlightList) {
        ContactListFragment contactListFragment = new ContactListFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_CHOICE_MODE, highlightList ?
                ListView.CHOICE_MODE_SINGLE :
                ListView.CHOICE_MODE_NONE);
        contactListFragment.setArguments(args);
        return contactListFragment;
    }


    public ContactListFragment() {
        // Required empty public constructor
        super();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Fragment parent = getParentFragment();
        Object objectToCast = parent != null ? parent : activity;
        try {
            listener = (ContactListFragmentListener) objectToCast;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(objectToCast.getClass().getSimpleName()
                    + " must implement ContactListFragmentListener");
        }

        animationDuration = activity.getResources().getInteger(
                android.R.integer.config_shortAnimTime);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // View holder here is not necessarily required since not accessing the
        // list view after this method. But including for example purposes
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        configureAdapter(holder.list);
        holder.list.setOnItemClickListener(this);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initialize the loader
        ContactLoaderCallbacks.initLoader(getActivity(), getLoaderManager(), this,
                ContactListAdapter.PROJECTION);
    }


    public void setHighlightList(boolean highlightList){
        this.listChoiceMode = highlightList ?
                ListView.CHOICE_MODE_SINGLE :
                ListView.CHOICE_MODE_NONE;
        ViewHolder holder = getViewHolder();
        if (holder != null) {
            configureAdapter(holder.list);
        }
    }


    private void configureAdapter(final ListView list) {

        ContactListAdapter adapter = new ContactListAdapter(getActivity());

        list.setAdapter(adapter);
        list.setChoiceMode(listChoiceMode);
        Log.d(TAG, "Configure adapter: " + selectedItem);
        list.setSelection(selectedItem);
        list.setItemChecked(selectedItem, true);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "saving selected item state: " + selectedItem);
        outState.putInt(STATE_SELECTION, selectedItem);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedItem = position;
        ListView list = (ListView) parent;
        list.setItemChecked(position, true);
        listener.editContact(id);
    }


    @Override
    public void onContactLoadComplete(final Cursor cursor) {
        ViewHolder holder = getViewHolder();
        if (holder == null) {
            return;
        }

        ContactListAdapter adapter = (ContactListAdapter) holder.list.getAdapter();
        adapter.swapCursor(cursor);

        showList(holder);
    }


    private void showList(ViewHolder holder) {
        // cross fade only if list is not already showing
        if (holder.list.getVisibility() != View.VISIBLE) {
            crossFadeViews(holder.list, holder.progressView);
        }
        holder.progressView.setVisibility(View.GONE);
    }


    private void showProgress(ViewHolder holder) {
        // cross fade only if list is currently showing
        if (holder.list.getVisibility() == View.VISIBLE) {
            crossFadeViews(holder.progressView, holder.list);
        }
    }


    private void crossFadeViews(final View fadeInView, final View fadeOutView) {
        fadeInView.setAlpha(0f);
        fadeInView.setVisibility(View.VISIBLE);

        ViewCompat.animate(fadeInView)
                .alpha(1f)
                .setDuration(animationDuration)
                .setListener(null)
                .withLayer();

        ViewCompat.animate(fadeOutView)
                .alpha(0f)
                .setDuration(animationDuration)
                .setListener(null)
                .withLayer()
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        fadeOutView.setVisibility(View.GONE);
                    }
                });
    }


    private ViewHolder getViewHolder() {
        View view = getView();
        return view != null ? (ViewHolder) view.getTag() : null;
    }


    /* package */ static class ViewHolder {
        final ListView list;
        final View progressView;

        ViewHolder(View view) {
            list = (ListView) view.findViewById(R.id.list);
            progressView = view.findViewById(R.id.progress);
            list.setEmptyView(view.findViewById(R.id.empty));
        }
    }
}
