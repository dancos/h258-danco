package com.example.danco.homework8.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.danco.homework8.R;
import com.example.danco.homework8.provider.ContactContract;

/**
 * Created by danco on 3/7/15.
 */
public class ContactListAdapter extends CursorAdapter {

    public static final String[] PROJECTION = new String[] {
            ContactContract.Columns._ID,
            ContactContract.Columns.NAME,
            ContactContract.Columns.ADDRESS,
            ContactContract.Columns.BIRTH_DATE
    };

    public static final int ID_POS = 0;
    public static final int NAME_POS = 1;
    public static final int ADDRESS_POS = 2;
    public static final int BIRTH_DATE_POS = 3;


    public ContactListAdapter(Context context) {
        // null indicates no cursor at this time.
        // 0 sets flags indicating loader managing cursor. Essentially should never be anything else
        super(context, null, 0);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Unless you are supporting different types of views, just return a view here.
        // This is called from getView when the convert view is null
        View view = LayoutInflater.from(context).inflate(R.layout.contact_list_item, parent, false);
        view.setTag(new ViewHolder(view));

        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // cursor already in position
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.contactName.setText(cursor.getString(NAME_POS));
    }


    /* package */
    static class ViewHolder {
        TextView contactName;

        ViewHolder(View view) {
            contactName = (TextView) view.findViewById(R.id.contactName);
        }
    }
}
