package com.example.danco.homework8.fragment;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.danco.homework8.R;
import com.example.danco.homework8.activity.AddContactActivity;
import com.example.danco.homework8.activity.ContactDetailActivity;
import com.example.danco.homework8.provider.ContactContract;
import com.example.danco.homework8.service.ContactService;

import java.util.Date;

/**
 * Master/Detail fragment for Contacts
 */
public class ContactsFragment extends Fragment
        implements ContactListFragment.ContactListFragmentListener {

    private static final String LIST_FRAG = "contactListFrag";
    private static final String DETAIL_FRAG = "contactDetailFrag";
    private static final int ADD_REQUEST = 105;

    private boolean hasDetailFragment = false;

    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    public ContactsFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Must call this in onCreate to see action bar item
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        hasDetailFragment = view.findViewById(R.id.contactDetailContainer) != null;

        if (savedInstanceState == null) {
            ContactListFragment contactListFragment =
                    ContactListFragment.newInstance(hasDetailFragment);
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.contactListContainer, contactListFragment, LIST_FRAG)
                    .commit();
        }
        else {
            ContactListFragment contactsListFragment = (ContactListFragment)
                    getChildFragmentManager().findFragmentByTag(LIST_FRAG);
            contactsListFragment.setHighlightList(hasDetailFragment);
        }

        if (hasDetailFragment) {
            ContactDetailFragment contactDetailFragment = (ContactDetailFragment)
                    getChildFragmentManager().findFragmentByTag(DETAIL_FRAG);
            if (contactDetailFragment == null) {
                contactDetailFragment = ContactDetailFragment.newInstance(0);
                getChildFragmentManager()
                        .beginTransaction()
                        .add(R.id.contactDetailContainer, contactDetailFragment, DETAIL_FRAG)
                        .commit();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // Adding add here since this fragment will always show the child. Also by placing
        // it here it limits the interaction with list fragment to just notifying
        // of the new item to add to the data set
        inflater.inflate(R.menu.menu_fragment_contacts, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        switch (item.getItemId()) {
            case R.id.add_contact:
                startActivityForResult(AddContactActivity.buildIntent(getActivity()), ADD_REQUEST);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADD_REQUEST && data != null) {
                String name = data.getStringExtra(AddContactActivity.EXTRA_NAME);

                // Persist the new name
                ContactService.startActionInsert(getActivity(), buildContentValues(name));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void editContact(long contactId) {
        if (hasDetailFragment) {
            ContactDetailFragment contactDetailFragment =
                    ContactDetailFragment.newInstance(contactId);
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contactDetailContainer, contactDetailFragment, DETAIL_FRAG)
                    .commit();
        }
        else {
            startActivity(ContactDetailActivity.buildIntent(getActivity(), contactId));
        }
    }


    private ContentValues buildContentValues(String name) {
        ContentValues values = new ContentValues();

        values.put(ContactContract.Columns.NAME, name);
        values.put(ContactContract.Columns.ADDRESS, "");
        values.put(ContactContract.Columns.BIRTH_DATE, new Date().getTime());
        return values;
    }


}
