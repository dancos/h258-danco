package com.example.danco.homework8.loader;

/**
 * Created by danco on 3/7/15.
 */
public final class LoaderIds {
    /**
     * prevent instantiation
     */
    private LoaderIds() {}

    public static final int CONTACT_LOADER = 100;
}
